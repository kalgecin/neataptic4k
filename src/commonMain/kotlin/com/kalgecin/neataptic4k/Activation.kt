package com.kalgecin.neataptic4k

import kotlin.math.*

public enum class Activation {
    LOGISTIC { //(0, 1)
        override fun calc(x: Double, derivate: Boolean): Double {
            val fx = 1 / (1 + exp(-x))
            return if (!derivate) fx else fx * (1 - fx)
        }
    },
    TANH { //(-1, 1)
        override fun calc(x: Double, derivate: Boolean): Double {
            if (derivate) {
                return 1 - tanh(x).pow(2)
            }
            return tanh(x)
        }
    },
    IDENTITY { // (-Inf, Inf)
        override fun calc(x: Double, derivate: Boolean): Double {
            return if (derivate) 1.0 else x
        }
    },
    STEP { // {0, 1}
        override fun calc(x: Double, derivate: Boolean): Double {
            return if (derivate) 0.0 else if (x > 0) 1.0 else 0.0
        }
    },
    RELU { //[0, Inf)
        override fun calc(x: Double, derivate: Boolean): Double {
            if (derivate) {
                return if (x > 0) 1.0 else 0.0
            }
            return max(x, 0.0)
        }
    },
    SOFTSIGN { // (-1, 1)
        override fun calc(x: Double, derivate: Boolean): Double {
            val d = 1.0 + abs(x)
            return if (derivate) x / d.pow(2) else x / d
        }
    },
    SINUSOID { // [-1, 1]
        override fun calc(x: Double, derivate: Boolean): Double {
            return if (derivate) cos(x) else sin(x)
        }
    },
    GAUSSIAN { // (0, 1]
        override fun calc(x: Double, derivate: Boolean): Double {
            val d = exp(-x.pow(2))
            return if (derivate) -2 * x * d else d
        }
    },
    BENT_IDENTITY { //(-Inf, Inf)
        override fun calc(x: Double, derivate: Boolean): Double {
            val d = sqrt(x.pow(2) + 1)
            if (derivate) {
                return x / (2 * d) + 1
            }
            return (d - 1) / 2 + x
        }
    },
    BIPOLAR { // 1 or -1
        override fun calc(x: Double, derivate: Boolean): Double {
            return if (derivate) 0.0 else if (x > 0) 1.0 else -1.0
        }
    },
    BIPOLAR_SIGMOID { //
        override fun calc(x: Double, derivate: Boolean): Double {
            val d = 2 / (1 + exp(-x)) - 1
            if (derivate) {
                return 0.5 * (1 + d) * (1 - d)
            }
            return d
        }
    },
    HARD_TANH {
        override fun calc(x: Double, derivate: Boolean): Double {
            if (derivate) {
                return if (x > -1 && x < 1) 1.0 else 0.0
            }
            return max(-1.0, min(1.0, x))
        }
    },
    ABSOLUTE {
        override fun calc(x: Double, derivate: Boolean): Double {
            if (derivate) {
                return if (x < 0) -1.0 else 1.0
            }
            return abs(x)
        }
    },
    INVERSE {
        override fun calc(x: Double, derivate: Boolean): Double {
            return if (derivate) -1.0 else 1 - x
        }
    },
    SELU {
        override fun calc(x: Double, derivate: Boolean): Double {
            val alpha = 1.6732632423543772848170429916717
            val scale = 1.0507009873554804934193349852946
            val fx = if (x > 0) x else alpha * exp(x) - alpha
            return if (derivate) if (x > 0) scale else (fx + alpha) * scale else fx * scale
        }
    };

    public abstract fun calc(x: Double, derivate: Boolean = false): Double
}

package com.kalgecin.neataptic4k

import com.kalgecin.neataptic4k.layer.*

public class Architect {
    public companion object {
        public fun createPerceptron(vararg layers: Int): Network {
            if (layers.size < 3) {
                throw RuntimeException("You have to specify at least 3 layers")
            }
            val nodes: MutableList<NodeGroup> = ArrayList()
            var last = NodeGroup(layers[0])
            nodes.add(last)
            for (i in 1 until layers.size) {
                val current = NodeGroup(layers[i])
                nodes.add(current)
                last.connect(current, ConnectionType.ALL_TO_ALL)
                last = current
            }
            return construct(nodes)
        }

        public fun construct(list: List<Any>): Network {
            var nodes: MutableList<Node> = ArrayList()
            for (elem in list) when (elem) {
                is Layer -> for (node in elem.nodeGroups) nodes.addAll(node.nodes)
                is NodeGroup -> nodes.addAll(elem.nodes)
                is Node -> nodes.add(elem)
            }
            val inputs: MutableList<Node> = ArrayList()
            val outputs: MutableList<Node> = ArrayList()
            var input = 0
            var output = 0
            for (i in nodes.indices.reversed()) {
                if (nodes[i].type == NodeTypeEnum.OUTPUT || nodes[i].connections.outputs.size + nodes[i].connections.gated.size == 0) {
                    nodes[i].type = NodeTypeEnum.OUTPUT
                    output++
                    outputs.add(nodes[i])
                    nodes.removeAt(i)
                } else if (nodes[i].type == NodeTypeEnum.INPUT || nodes[i].connections.inputs.size == 0) {
                    nodes[i].type = NodeTypeEnum.INPUT
                    input++
                    inputs.add(nodes[i])
                    nodes.removeAt(i)
                }
            }
            if (input == 0 || output == 0) {
                throw RuntimeException("Given nodes have no clear input/output node!")
            }

            val network = Network(input, output)
            network.connections.clear()
            network.nodes.clear()
            network.selfConns.clear()

            val temp = ArrayList<Node>()
            temp.addAll(inputs)
            temp.addAll(nodes)
            temp.addAll(outputs)
            nodes = temp
            for (node in nodes) {
                network.connections.addAll(node.connections.outputs)
                network.gates.addAll(node.connections.gated)
                if (node.connections.self.weight != 0.0) {
                    network.selfConns.add(node.connections.self)
                }
            }
            network.nodes = nodes
            return network
        }

        public fun createRandom(input: Int, hidden: Int, output: Int, options: Map<Option, Int> = emptyMap()): Network {
            val connections = options[Option.CONNECTIONS] ?: hidden * 2
            val backConnections = options[Option.BACK_CONNECTIONS] ?: 0
            val selfConnections = options[Option.SELF_CONNECTIONS] ?: 0
            val gates = options[Option.GATES] ?: 0

            val network = Network(input, output)
            for (i in 0 until hidden) network.mutate(MutationType.ADD_NODE)
            for (i in 0 until connections - hidden) network.mutate(MutationType.ADD_CONN)
            for (i in 0 until backConnections) network.mutate(MutationType.ADD_BACK_CONN)
            for (i in 0 until selfConnections) network.mutate(MutationType.ADD_SELF_CONN)
            for (i in 0 until gates) network.mutate(MutationType.ADD_GATE)
            return network
        }

        public fun createGRU(vararg layers: Int): Network {
            val inputLayer = NodeGroup(layers[0])
            val outputLayer = NodeGroup(layers[layers.size - 1])

            val blocks = layers.copyOfRange(1, layers.size - 1)
            val nodes = arrayListOf<NodeGroup>()
            nodes.add(inputLayer)

            var previous = inputLayer
            for (block in blocks) {
                val layer = GRU(block)
                previous.connect(layer, null, 0.0)
                previous = layer
                nodes.add(layer)
            }
            previous.connect(outputLayer, null, 0.0)
            nodes.add(outputLayer)
            return construct(nodes)
        }

        public fun createHopfield(size: Int): Network {
            val input = NodeGroup(size)
            val output = NodeGroup(size)

            input.connect(output, ConnectionType.ALL_TO_ALL)

            input.set(NodeValues(type = NodeTypeEnum.INPUT))
            output.set(NodeValues(squash = Activation.STEP, type = NodeTypeEnum.OUTPUT))

            val nodes = mutableListOf<Any>()
            nodes.add(input)
            nodes.add(output)
            return construct(nodes)
        }

        public fun createNARX(
            inputSize: Int,
            hiddenLayers: IntArray,
            outputSize: Int,
            previousInput: Int,
            previousOutput: Int
        ): Network {
            val nodes = mutableListOf<Any>()
            val input = Dense(inputSize)
            val inputMemory = Memory(inputSize, previousInput)
            val hidden = mutableListOf<Layer>()
            val output = Dense(outputSize)
            val outputMemory = Memory(outputSize, previousOutput)

            nodes.add(input)
            nodes.add(outputMemory)
            for (i in hiddenLayers.indices) {
                val hiddenLayer = Dense(hiddenLayers[i])
                hidden.add(hiddenLayer)
                nodes.add(hiddenLayer)
                if (i - 1 < hidden.size && i >= 1 && hidden.getOrNull(i - 1) != null) {
                    hidden[i - 1].connect(hiddenLayer, ConnectionType.ALL_TO_ALL)
                }
            }

            nodes.add(inputMemory)
            nodes.add(output)

            input.connect(hidden[0], ConnectionType.ALL_TO_ALL)
            input.connect(inputMemory, ConnectionType.ONE_TO_ONE, 1.0)
            inputMemory.connect(hidden[0], ConnectionType.ALL_TO_ALL)
            hidden[hidden.size - 1].connect(output, ConnectionType.ALL_TO_ALL)
            output.connect(outputMemory, ConnectionType.ONE_TO_ONE, 1.0)
            outputMemory.connect(hidden[0], ConnectionType.ALL_TO_ALL)

            input.set(NodeValues(type = NodeTypeEnum.INPUT))
            output.set(NodeValues(type = NodeTypeEnum.OUTPUT))
            return construct(nodes)
        }

        public fun createLSTM(options: Map<Option, Boolean> = mapOf(), vararg layers: Int): Network {
            val last = layers.last()
            val oututLayer = NodeGroup(last)
            oututLayer.set(NodeValues(type = NodeTypeEnum.OUTPUT))

            val inputLayer = NodeGroup(layers[0])
            inputLayer.set(NodeValues(type = NodeTypeEnum.INPUT))

            val blocks = layers.copyOfRange(1, layers.size - 1)
            val nodes = mutableListOf<NodeGroup>()
            nodes.add(inputLayer)

            var previous = inputLayer
            for (i in blocks.indices) {
                val block = blocks[i]

                val inputGate = NodeGroup(block)
                val forgetGate = NodeGroup(block)
                val memoryCell = NodeGroup(block)
                val outputGate = NodeGroup(block)
                val outputBlock = if (i == blocks.size - 1) oututLayer else NodeGroup(block)

                inputGate.set(NodeValues(bias = 1.0))
                forgetGate.set(NodeValues(bias = 1.0))
                outputGate.set(NodeValues(bias = 1.0))

                val input = previous.connect(memoryCell, ConnectionType.ALL_TO_ALL)
                previous.connect(inputGate, ConnectionType.ALL_TO_ALL)
                previous.connect(outputGate, ConnectionType.ALL_TO_ALL)
                previous.connect(forgetGate, ConnectionType.ALL_TO_ALL)

                memoryCell.connect(inputGate, ConnectionType.ALL_TO_ALL)
                memoryCell.connect(forgetGate, ConnectionType.ALL_TO_ALL)
                memoryCell.connect(outputGate, ConnectionType.ALL_TO_ALL)
                val forget = memoryCell.connect(memoryCell, ConnectionType.ONE_TO_ONE)
                val output = memoryCell.connect(outputBlock, ConnectionType.ALL_TO_ALL)

                inputGate.gate(input, GatingType.INPUT)
                forgetGate.gate(forget, GatingType.SELF)
                outputGate.gate(output, GatingType.OUTPUT)

                if (options.getOrElse(Option.INPUT_TO_DEEP, { true }) && i > 0) {
                    inputGate.gate(inputLayer.connect(memoryCell, ConnectionType.ALL_TO_ALL), GatingType.INPUT)
                }

                if (options.getOrElse(Option.MEMORY_TO_MEMORY, { false })) {
                    inputGate.gate(memoryCell.connect(memoryCell, ConnectionType.ALL_TO_ELSE), GatingType.INPUT)
                }

                if (options.getOrElse(Option.OUTPUT_TO_MEMORY, { false })) {
                    inputGate.gate(oututLayer.connect(memoryCell, ConnectionType.ALL_TO_ALL), GatingType.INPUT)
                }

                if (options.getOrElse(Option.OUTPUT_TO_GATES, { false })) {
                    oututLayer.connect(inputGate, ConnectionType.ALL_TO_ALL)
                    oututLayer.connect(forgetGate, ConnectionType.ALL_TO_ALL)
                    oututLayer.connect(outputGate, ConnectionType.ALL_TO_ALL)
                }

                nodes.add(inputGate)
                nodes.add(forgetGate)
                nodes.add(memoryCell)
                nodes.add(outputGate)
                if (i != blocks.size - 1) nodes.add(outputBlock)
                previous = outputBlock
            }
            if (options.getOrElse(Option.INPUT_TO_OUTPUT, { true })) {
                inputLayer.connect(oututLayer, ConnectionType.ALL_TO_ALL)
            }
            nodes.add(oututLayer)
            return construct(nodes)
        }

        public enum class Option { CONNECTIONS, BACK_CONNECTIONS, SELF_CONNECTIONS, GATES, MEMORY_TO_MEMORY, OUTPUT_TO_MEMORY, OUTPUT_TO_GATES, INPUT_TO_OUTPUT, INPUT_TO_DEEP }
    }
}

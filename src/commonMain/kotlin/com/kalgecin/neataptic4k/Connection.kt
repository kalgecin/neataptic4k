package com.kalgecin.neataptic4k

import kotlin.math.floor

public data class Connection(
    var from: Node,
    val to: Node,
    var weight: Double,

    var gater: Node? = null,
    var prevDeltaWeight: Double = 0.0,
    var totalDeltaWeight: Double = 0.0,
    var gain: Double = 1.0,
    var xTraceNodes: MutableList<Node> = arrayListOf(),
    var xTraceValues: MutableList<Double> = arrayListOf(),
    var eligibility: Double = 0.0
) {
    public companion object {
        internal fun getInnovationID(a: Int, b: Int): Int {
            return floor(0.5 * (a + b) * (a + b + 1) + b).toInt()
        }
    }
}

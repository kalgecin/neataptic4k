package com.kalgecin.neataptic4k

import kotlin.math.*
import kotlin.random.Random

internal enum class ConnectionType {
    ALL_TO_ALL, ALL_TO_ELSE, ONE_TO_ONE;
}

internal data class ConnectionListGroup(
        val inputs: MutableList<Connection> = arrayListOf(),
        val out: MutableList<Connection> = arrayListOf(),
        val self: MutableList<Connection> = arrayListOf()
)

internal enum class GatingType {
    OUTPUT,
    INPUT,
    SELF
}

public enum class SelectionType(internal val size: Int = 0, internal val probability: Double = 0.0, internal val power: Int = 0) {
    FITNESS_PROPORTIONATE {
        override fun select(population: Array<Network>): Network {
            var totalFitness = 0.0
            var minimalFitness = 0.0
            for (network in population) {
                val score = network.score
                minimalFitness = min(score, minimalFitness)
                totalFitness += score
            }

            minimalFitness = abs(minimalFitness)
            totalFitness += minimalFitness * population.size
            val random = Random.nextDouble() * totalFitness
            var value = 0.0
            for (genome in population) {
                value += genome.score + minimalFitness
                if (random < value) return genome
            }
            return population.random()
        }
    },
    POWER(4) {
        override fun select(population: Array<Network>): Network {
            return population[floor(Random.nextDouble().pow(power) * population.size).toInt()]
        }
    },
    TOURNAMENT(5, 0.5) {
        override fun select(population: Array<Network>): Network {
            if (size > population.size) {
                throw RuntimeException("Your tournament size should be lower than the population size, please change methods.selection.TOURNAMENT.size")
            }
            val individuals = arrayListOf<Network>()
            repeat(size) { individuals.add(population.random()) }
            individuals.sortBy { it.score }

            repeat(size) { i ->
                if (Random.nextDouble() < probability || i == size - 1) {
                    return individuals[i]
                }
            }
            return population[0]
        }
    };

    constructor(power: Int) : this(0, 0.0, power)

    public abstract fun select(population: Array<Network>): Network
}

public enum class CrossOverType(private val config: DoubleArray) {
    SINGLE_POINT(doubleArrayOf(0.4)),
    TWO_POINT(doubleArrayOf(0.4, 0.9)),
    UNIFORM(doubleArrayOf()),
    AVERAGE(doubleArrayOf())
}

public enum class Cost {
    CROSS_ENTROPY {
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            val error = output.indices
                    .map { -(target[it] * ln(max(output[it], 1e-15)) + (1 - target[it]) * ln(1.0 - max(output[it], 1e-15))) }
                    .sum()
            return error / output.size
        }
    },
    MSE {
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            val error = output.indices
                    .map { i -> (target[i] - output[i]).pow(2) }
                    .sum()
            return error / output.size
        }
    },
    BINARY {
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            return output.indices
                    .map { i -> if (round(target[i] * 2.0) != round(output[i] * 2.0)) 1.0 else 0.0 }
                    .sum()
        }
    },
    MAE {
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            val error = output.indices
                    .map { i -> abs(target[i] - output[i]) }
                    .sum()
            return error / output.size
        }
    },
    MAPE {
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            val error = output.indices
                    .map { i -> abs((output[i] - target[i]) / max(target[i], 1e-15)) }
                    .sum()
            return error / output.size
        }
    },
    MSLE {
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            return output.indices
                    .map { i -> ln(max(target[i], 1e-15)) - ln(max(output[i], 1e-15)) }
                    .sum()
        }
    },
    HINGE {
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            return output.indices
                    .map { i -> max(0.0, 1.0 - target[i] * output[i]) }
                    .sum()
        }
    },
    SMCEL {
        //Softmax Cross-entropy with Logits
        override fun calc(target: DoubleArray, output: DoubleArray): Double {
            return -softmax(output).map { ln(it) }.mapIndexed { i,v -> target[i] * v}.sum()
        }

        private fun softmax(arr: DoubleArray): DoubleArray {
            val expArr = arr.map { exp(it) }
            val sum = expArr.sum()
            return expArr.map { it / sum }.toDoubleArray()
        }
    };

    public abstract fun calc(target: DoubleArray, output: DoubleArray): Double
}

/**
 * @param populationSize total number of networks in the population
 * @param elitism number of top networks to leave in the population as is
 * @param provenance number of provenance networks to add to each generation
 * @param mutationRate probability of mutating a network
 * @param mutationAmount number of times to mutate a network if it's selected for mutation
 * @param selection algorithm to use
 * @param mutationTypes array of [MutationType] to choose from when performing mutation
 * @param maxNodes allowed in the mutated network
 * @param maxConns allowed in the mutated network
 * @param maxGates allowed in the mutated network
 * @param isEqual whether to treat networks as equal during mutation
 * @param isClear whether to clear the network between each evaluation
 * @param error stop evolving once error is less than or equal to this value
 * @param growthPenalty this is a measure of how much to favour smaller networks, favours smaller networks that perform the same or close to larger networks
 * @param amount number of times to loop over fitness calculation. Helps smooth out error rate in some cases
 * @param cost [Cost] method to use when calculating the fitness of a network
 * @param iterations number of iterations to perform
 * @param log number of iterations to print out statistics
 * @param network used to add provenance to new populations
 * @param crossover [CrossOverType] to use when breeding networks
 */
public data class EvolveOptions(
        var populationSize: Int = 50,
        var elitism: Int = 0,
        var provenance: Int = 0,
        var mutationRate: Double = 0.3,
        var mutationAmount: Int = 1,
        var selection: SelectionType = SelectionType.POWER,
        var mutationTypes: Array<MutationType> = MutationType.FFW,
        var maxNodes: Int = Int.MAX_VALUE,
        var maxConns: Int = Int.MAX_VALUE,
        var maxGates: Int = Int.MAX_VALUE,
        var isEqual: Boolean = false,
        var isClear: Boolean = false,
        var error: Double = Double.NaN,
        var growthPenalty: Double = 0.0001,
        var amount: Int = 1,
        var cost: Cost = Cost.MSE,
        var iterations: Int = -1,
        var log: Int = 0,
        var network: Network? = null,
        val crossover: Array<CrossOverType> = arrayOf(
                CrossOverType.SINGLE_POINT, CrossOverType.TWO_POINT, CrossOverType.UNIFORM, CrossOverType.AVERAGE
        )
)

public data class DataEntry(val input: DoubleArray, val output: DoubleArray)

public data class TrainOptions(
        val rate: Double = 0.3,
        val error: Double = 0.05,
        val dropout: Double = 0.0,
        val momentum: Double = 0.0,
        val batchSize: Int = 1,
        val ratePolicy: Rate = Rate.FIXED(),
        val cost: Cost = Cost.MSE,
        var iterations: Int = -1,
        val crossValidate: Boolean = false,
        val crossValidateTestSize: Int = -1,
        val crossValidateTestError: Double = Double.NaN,
        val clear: Boolean = false,
        val shuffle: Boolean = false,
        val log: Int = 0
)

public abstract class Rate {
    public abstract fun calc(baseRate: Double, iterations: Int): Double

    public class FIXED : Rate() {
        override fun calc(baseRate: Double, iterations: Int): Double = baseRate
    }

    public class STEP constructor(private val stepSize: Int = 100, private val gamma: Double = 0.9) :
            Rate() {

        public constructor(gamma: Double) : this(100, gamma)

        override fun calc(baseRate: Double, iterations: Int): Double {
            return baseRate * gamma.pow(floor(iterations.toDouble() / stepSize))
        }

    }

    public class EXP constructor(private val gamma: Double = 0.999) : Rate() {
        override fun calc(baseRate: Double, iterations: Int): Double = baseRate * gamma.pow(iterations)

    }

    public class INV constructor(private val gamma: Double = 0.001, private val power: Double = 2.0) : Rate() {
        override fun calc(baseRate: Double, iterations: Int): Double = baseRate * (1 + gamma * iterations).pow(-power)
    }
}

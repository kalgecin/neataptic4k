package com.kalgecin.neataptic4k

public enum class MutationType(
    public val min: Double = 0.0, public val max: Double = 0.0,
    public val mutateOutput: Boolean = false, public val allowed: Array<Activation> = arrayOf(),
    public val keepGates: Boolean = false) {
    ADD_NODE, SUB_NODE(keepGates = true),
    ADD_CONN, SUB_CONN,
    MOD_WEIGHT(-1.0, 1.0), MOD_BIAS(-1.0, 1.0),
    MOD_ACTIVATION(
        mutateOutput = true,
        allowed = arrayOf<Activation>(
            Activation.LOGISTIC,
            Activation.TANH,
            Activation.RELU,
            Activation.IDENTITY,
            Activation.STEP,
            Activation.SOFTSIGN,
            Activation.SINUSOID,
            Activation.GAUSSIAN,
            Activation.BENT_IDENTITY,
            Activation.BIPOLAR,
            Activation.BIPOLAR_SIGMOID,
            Activation.HARD_TANH,
            Activation.ABSOLUTE,
            Activation.INVERSE,
            Activation.SELU
        )
    ),
    ADD_SELF_CONN, SUB_SELF_CONN,
    ADD_GATE, SUB_GATE,
    ADD_BACK_CONN, SUB_BACK_CONN,
    SWAP_NODES(keepGates = true, mutateOutput = true);

    public companion object {
        public val ALL: Array<MutationType> = arrayOf(
            ADD_NODE,
            SUB_NODE,
            ADD_CONN,
            SUB_CONN,
            MOD_WEIGHT,
            MOD_BIAS,
            MOD_ACTIVATION,
            ADD_GATE,
            SUB_GATE,
            ADD_SELF_CONN,
            SUB_SELF_CONN,
            ADD_BACK_CONN,
            SUB_BACK_CONN,
            SWAP_NODES
        )
        public val FFW: Array<MutationType> = arrayOf(
            ADD_NODE,
            SUB_NODE,
            ADD_CONN,
            SUB_CONN,
            MOD_WEIGHT,
            MOD_BIAS,
            MOD_ACTIVATION,
            SWAP_NODES
        )
    }
}

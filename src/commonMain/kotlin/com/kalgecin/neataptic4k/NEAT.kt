package com.kalgecin.neataptic4k

import kotlin.random.Random

public class NEAT(
    private val input: Int,
    private val output: Int,
    public val fitnesFunction: ((Network) -> Double)?,
    public val options: EvolveOptions = EvolveOptions()
) {
    public var generation: Int = 0
    public var population: Array<Network> = arrayOf()

    init {
        this.createPool(options.network)
    }

    private fun createPool(template: Network?) {
        population = (0 until options.populationSize).map {
            val copy = template?.copy() ?: Network(input, output)
            copy.score = Double.NaN
            copy
        }.toTypedArray()
    }

    /**
     * Evaluates, selects, breeds and mutates population
     * @return best Network by score of population before mutation
     */
    public fun evolve(): Network {
        if (this.population.last().score.isNaN()) evaluateNeat(population, fitnesFunction, options)
        sort()
        val fittest = population[0].copy()
        fittest.score = population[0].score

        val elitists = population.take(options.elitism)

        val newPopulation = (0 until options.provenance).mapNotNull { options.network?.copy() }

        val breed = (0 until options.populationSize - options.elitism - options.provenance).map { getOffspring() }

        population = newPopulation.union(breed).toTypedArray()
        mutate()

        population = population.union(elitists).toTypedArray()

        population.forEach { it.score = Double.NaN }

        generation++
        return fittest
    }

    public fun sort(): Unit = population.sortByDescending { it.score }

    public fun getOffspring(): Network = Network.crossover(getParent(), getParent(), options.isEqual)

    public fun mutate(): Unit = this.population
        .filter { Random.nextDouble() <= options.mutationRate }
        .forEach { network ->
            repeat(options.mutationAmount) {
                selectMutationMethod(network)?.let { network.mutate(it) }
            }
        }

    private fun getParent(): Network {
        return options.selection.select(population)
    }

    internal fun selectMutationMethod(genome: Network): MutationType? {
        val mutationMethod = options.mutationTypes.random()

        return if (mutationMethod == MutationType.ADD_NODE && genome.nodes.size >= options.maxNodes)
            null
        else if (mutationMethod == MutationType.ADD_CONN && genome.connections.size >= options.maxConns)
            null
        else if (mutationMethod == MutationType.ADD_GATE && genome.gates.size >= options.maxGates)
            null
        else mutationMethod
    }

    public fun import(population: Array<Network>) {
        this.population = population
        this.options.populationSize = population.size
    }
}

internal expect fun NEAT.evaluateNeat(population: Array<Network>, fitnessFunction: ((Network) -> Double)?, options: EvolveOptions)
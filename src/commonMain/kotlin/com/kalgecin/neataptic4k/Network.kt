package com.kalgecin.neataptic4k

import kotlinx.serialization.Serializable
import kotlin.js.JsName
import kotlin.math.*
import kotlin.random.Random

public class Network(public val input: Int, public val output: Int) {
    public var gates: MutableList<Connection> = arrayListOf()
    public var score: Double = Double.NaN
    public var nodes: MutableList<Node>
    public var connections: MutableList<Connection> = arrayListOf()
    public var selfConns: MutableList<Connection> = arrayListOf()
    public var id: Long = Random.nextLong()
    private var dropout: Double = 0.0

    init {
        this.nodes = (0 until this.input + this.output)
            .map { i ->
                val type = if (i < input) NodeTypeEnum.INPUT else NodeTypeEnum.OUTPUT
                Node(type)
            }.toMutableList()

        for (i in 0 until this.input) {
            for (j in this.input until (this.output + this.input)) {
                //https://gitlab.com/kalgecin/neataptic4k/-/wikis/Network#weight-initialization
                val weight = Random.nextDouble(0.0, input.toDouble()) * sqrt(2.0 / this.input)
                connect(nodes[i], nodes[j], weight)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        return other is Network && other.id == this.id
    }

    override fun hashCode(): Int {
        return this.id.hashCode()
    }


    /**
     * Connects the `from` node to the `to` node with the specified weight
     */
    private fun connect(from: Node, to: Node, weight: Double?): Connection {
        val connection = from.connect(to, weight)
        if (from != to) {
            this.connections.add(connection)
        } else {
            this.selfConns.add(connection)
        }
        return connection
    }

    /**
     * Gate a connection wwith a node
     */
    private fun gate(node: Node, connection: Connection) {
        if (!nodes.contains(node)) {
            throw RuntimeException("This node is not part of the network!")
        } else if (connection.gater != null) {
            return
        }
        node.gate(connection)
        gates.add(connection)
    }

    public data class Conn(val to: Int, val from: Int, val weight: Double, val gater: Int)

    override fun toString(): String {
        return "Network{" +
                "input=" + this.input +
                ", output=" + this.output +
                ", gates=" + this.gates +
                ", nodes=" + this.nodes +
                ", connections=" + this.connections +
                ", selfConns=" + this.selfConns +
                '}'
    }

    /**
     * Evolves the network trough NEAT to reach a lower error on the provided dataset
     *
     * @return final error of the network
     */
    @JsName("evolve")
    public fun evolve(set: Array<DataEntry>, options: EvolveOptions = EvolveOptions()): Double {
        if (set[0].input.size != this.input || set[0].output.size != this.output) {
            throw RuntimeException("Dataset input/output size should be same as network input/output size!")
        }

        var targetError = if (options.error.isNaN()) 0.05 else options.error

        if (options.iterations == -1 && options.error.isNaN()) {
            throw RuntimeException("At least one of the following options must be specified: error, iterations")
        } else if (options.error.isNaN()) {
            targetError = -1.0
        } else if (options.iterations == -1) {
            options.iterations = 0
        }

        val fitnessFunction = { genome: Network ->
            var score = (0 until options.amount)
                .map { -genome.test(set.asList(), options.cost) }
                .sum()
            score -= genome.sizePenalty(options.growthPenalty)
            score = if (score.isNaN()) -Double.MAX_VALUE else score
            score / options.amount
        }
        options.network = this
        val neat = NEAT(input, output, fitnessFunction, options)

        var bestFitness = fitnessFunction(this)
        var error = bestFitness + sizePenalty(options.growthPenalty)
        var bestGenome: Network? = null

        while (error < -targetError && (options.iterations == 0 || neat.generation < options.iterations)) {
            val fittest = neat.evolve()
            val fitness = fittest.score
            error = fitness + fittest.sizePenalty(options.growthPenalty)
            if (fitness > bestFitness) {
                bestFitness = fitness
                bestGenome = fittest
            }
            if (options.log > 0 && neat.generation % options.log == 0) println("Iteration: ${neat.generation}; Fitness: $fitness; Error: ${-error}")
        }
        println("Iteration: ${neat.generation}; Fitness: $bestFitness; Error: ${-error}")

        if (bestGenome != null) {
            this.nodes = bestGenome.nodes
            this.connections = bestGenome.connections
            this.selfConns = bestGenome.selfConns
            this.gates = bestGenome.gates
            if (options.isClear) clear()
        }
        return -error
    }

    /**
     * Tests a set and returns the error
     */
    public fun test(set: List<DataEntry>, cost: Cost = Cost.MSE): Double {
        if (dropout != 0.0) {
            nodes
                .filter { it.type == NodeTypeEnum.HIDDEN || it.type == NodeTypeEnum.CONSTANT }
                .forEach { it.mask = 1 - dropout }
        }
        val error = set.sumByDouble { cost.calc(it.output, noTraceActivate(it.input)) }
        return error / set.size
    }

    /**
     * Activates the network without calculating eligibility traces and such. Results in faster activation, only
     * usable if not training as no backpropagation data is recorded
     *
     * @param input input array. array size must be equal to network input size
     *
     * @return result
     */
    public fun noTraceActivate(input: DoubleArray): DoubleArray {
        val output = mutableListOf<Double>()
        nodes.forEachIndexed { i, node ->
            when (node.type) {
                NodeTypeEnum.INPUT -> node.noTraceActivation(input[i])
                NodeTypeEnum.OUTPUT -> output.add(node.noTraceActivation())
                else -> node.noTraceActivation()
            }
        }
        return output.toDoubleArray()
    }

    /**
     * mutates the network with the given method
     */
    public fun mutate(method: MutationType) {
        when (method) {
            MutationType.ADD_NODE -> {
                val connection = connections.random()
                val gater = connection.gater
                disconnect(connection.from, connection.to)

                val toIndex = nodes.indexOf(connection.to)
                val node = Node(NodeTypeEnum.HIDDEN)

                node.mutate(MutationType.MOD_ACTIVATION)
                val minBound = min(toIndex, nodes.size - output)
                this.nodes.add(minBound, node)

                val newConn1 = connect(connection.from, node, null)
                val newConn2 = connect(node, connection.to, null)

                if (gater != null) {
                    gate(gater, if (Random.nextBoolean()) newConn1 else newConn2)
                }
            }
            MutationType.SUB_NODE -> {
                nodes.filter { it.type == NodeTypeEnum.HIDDEN }
                    .randomOrNull()
                    ?.let {
                        remove(it)
                    }
            }
            MutationType.ADD_CONN -> {
                val available = mutableListOf<Pair<Node, Node>>()
                repeat(nodes.size - output) { i ->
                    val node = nodes[i]
                    (max(i + 1, input) until nodes.size).asSequence()
                        .map { nodes[it] }
                        .filter { node2 -> !node.isProjectingTo(node2) }
                        .map { node2 -> Pair(node, node2) }
                        .forEach { available.add(it) }
                }

                if (available.size == 0) return
                val pair = available.random()
                this.connect(pair.first, pair.second, null)
            }
            MutationType.SUB_CONN -> {
                connections
                    .filter { it.from.connections.outputs.size > 1 && it.to.connections.inputs.size > 1 }
                    .filter { nodes.indexOf(it.to) > nodes.indexOf(it.from) }
                    .randomOrNull()
                    ?.let {
                        disconnect(it.from, it.to)
                    }
            }
            MutationType.MOD_WEIGHT -> {
                connections.union(selfConns).random().weight += Random.nextDouble(method.min, method.max)
            }
            MutationType.MOD_BIAS -> nodes.filter { it.type != NodeTypeEnum.INPUT }.random().mutate(method)
            MutationType.MOD_ACTIVATION -> {
                nodes.filter { !method.mutateOutput && input + output == nodes.size }
                    .randomOrNull()?.mutate(method)
            }
            MutationType.ADD_SELF_CONN -> {
                nodes.asSequence().drop(input)
                    .filter { it.connections.self.weight == 0.0 }
                    .toList().randomOrNull()
                    ?.let {
                        connect(it, it, null)
                    }
            }
            MutationType.SUB_SELF_CONN -> selfConns.randomOrNull()?.let { disconnect(it.from, it.to) }
            MutationType.ADD_GATE -> {
                val index = Random.nextInt(input, nodes.size)
                connections.union(selfConns)
                    .filter { it.gater == null }
                    .randomOrNull()
                    ?.let { gate(nodes[index], it) }
            }
            MutationType.SUB_GATE -> gates.randomOrNull()?.let { ungate(it) }
            MutationType.ADD_BACK_CONN -> {
                val available = arrayListOf<Pair<Node, Node>>()
                (input until nodes.size)
                    .forEach { i ->
                        val node = nodes[i]
                        (input until i).asSequence()
                            .map { nodes[it] }
                            .filter { node2 -> !node.isProjectingTo(node2) }
                            .map { node3 -> Pair(node, node3) }
                            .forEach { available.add(it) }
                    }

                available.randomOrNull()?.let {
                    connect(it.first, it.second, null)
                }
            }
            MutationType.SUB_BACK_CONN -> {
                this.connections
                    .filter { it.from.connections.outputs.size > 1 && it.to.connections.inputs.size > 1 }
                    .filter { nodes.indexOf(it.from) > nodes.indexOf(it.to) }
                    .randomOrNull()
                    ?.let { disconnect(it.from, it.to) }
            }
            MutationType.SWAP_NODES -> {
                if ((method.mutateOutput && this.nodes.size - this.input < 2) ||
                    (!method.mutateOutput && this.nodes.size - this.input - this.output < 2)
                ) return

                val outputSize = if (method.mutateOutput) 0 else output
                val index1 = Random.nextInt(input, nodes.size - outputSize)
                val index2 = Random.nextInt(input, nodes.size - outputSize)
                val node = this.nodes[index1]
                val node2 = this.nodes[index2]

                val biasTemp = node.bias
                val squashTemp = node.squash

                node.bias = node2.bias
                node.squash = node2.squash
                node2.bias = biasTemp
                node2.squash = squashTemp
            }
        }
    }

    /**
     * Disconnects the `from` node from the `to` node
     */
    private fun disconnect(from: Node, to: Node) {
        val connections = if (from == to) this.selfConns else this.connections
        for (i in connections.indices) {
            val connection = connections[i]
            if (connection.from == from && connection.to == to) {
                if (connection.gater != null) {
                    this.ungate(connection)
                }
                connections.removeAt(i)
                break
            }
        }
        from.disconnect(to)
    }

    /**
     * removes a node from the network
     */
    private fun remove(node: Node) {
        if (!nodes.contains(node)) {
            throw RuntimeException("This node does not exist in the network!")
        }

        val gaters = mutableListOf<Node>()
        disconnect(node, node)

        val inputs = mutableListOf<Node>()
        node.connections.inputs.reversed().forEach { connection ->
            val gater = connection.gater
            if (MutationType.SUB_NODE.keepGates && gater != null && gater != node) {
                gaters.add(gater)
            }
            inputs.add(connection.from)
            disconnect(connection.from, node)
        }

        val outputs = mutableListOf<Node>()
        node.connections.outputs.reversed().forEach { connection ->
            val gater = connection.gater
            if (MutationType.SUB_NODE.keepGates && gater != null && gater != node) {
                gaters.add(gater)
            }
            outputs.add(connection.to)
            disconnect(node, connection.to)
        }

        val connections = mutableListOf<Connection>()
        for (input in inputs) {
            for (output in outputs) {
                if (!input.isProjectingTo(output)) {
                    val conn = connect(input, output, null)
                    connections.add(conn)
                }
            }
        }

        for (gater in gaters) {
            if (connections.size == 0) break
            val conn = connections.random()
            gate(gater, conn)
            connections.remove(conn)
        }

        node.connections.gated.reversed().forEach { this.ungate(it) }

        disconnect(node, node)
        nodes.remove(node)
    }

    /**
     * Remove the gate of a connection
     */
    private fun ungate(connection: Connection) {
        val index = gates.indexOf(connection)
        if (index == -1) {
            throw RuntimeException("This connection is not gated!")
        }
        gates.removeAt(index)
        connection.gater?.ungate(connection)
    }

    /**
     * Sets the values to all nodes in this network
     */
    public fun set(values: NodeValues) {
        nodes.forEach { node ->
            node.bias = values.bias ?: node.bias
            node.squash = values.squash ?: node.squash
        }
    }

    /**
     * Train the given set to this network
     *
     * @param set training data
     * @param options
     *
     * @return final error
     */
    @JsName("train")
    public fun train(set: Array<DataEntry>, options: TrainOptions = TrainOptions()): Double {
        if (set[0].input.size != this.input || set[0].output.size != this.output) {
            throw RuntimeException(
                "Dataset input/output size should be same as network input/output size!\n" +
                        "${set[0].input.size} - ${this.input}; ${set[0].output.size} - ${this.output}"
            )
        }

        val targetError = options.error
        val cost = options.cost
        val baseRate = options.rate
        val dropout = options.dropout
        val momentum = options.momentum
        val batchSize = options.batchSize
        val ratePolicy = options.ratePolicy

        if (batchSize > set.size) {
            throw RuntimeException("Batch size must be smaller or equal to dataset length!")
        } else if (options.iterations == -1 && options.error.isNaN()) {
            throw RuntimeException("At least one of the following options must be specified: error, iterations")
        } else if (options.iterations == -1) {
            options.iterations = 0
        }

        this.dropout = dropout
        var trainSet = set.toMutableList()
        var testSet: List<DataEntry> = emptyList()

        if (options.crossValidate) {
            val numTrain = ceil((1.0 - options.crossValidateTestSize) * set.size).toInt()
            testSet = trainSet.subList(numTrain, trainSet.size)
            trainSet = trainSet.subList(0, numTrain)
        }

        var currentRate: Double
        var iteration = 0
        var error = 1.0

        while (error > targetError && (options.iterations == 0 || iteration < options.iterations)) {
            if (options.crossValidate && error <= options.crossValidateTestError) break
            iteration++

            currentRate = ratePolicy.calc(baseRate, iteration)

            if (options.crossValidate) {
                trainSet(trainSet, batchSize, currentRate, momentum, cost)
                if (options.clear) clear()
                error = test(testSet, cost)
                if (options.clear) clear()
            } else {
                error = trainSet(trainSet, batchSize, currentRate, momentum, cost)
                if (options.clear) clear()
            }
            if (options.shuffle) trainSet.shuffle()

            if (options.log > 0 && iteration % options.log == 0) println("Iteration: $iteration; Error: $error; Rate: $currentRate")
        }
        if (options.clear) clear()
        if (dropout != 0.0) {
            nodes.filter { node -> node.type == NodeTypeEnum.HIDDEN || node.type == NodeTypeEnum.CONSTANT }
                .forEach { node -> node.mask = 1 - dropout }
        }
        return error
    }

    /**
     * Performs one training epoch and returns the error
     */
    private fun trainSet(set: List<DataEntry>, batchSize: Int, rate: Double, momentum: Double, cost: Cost): Double {
        var errorSum = 0.0
        for (i in set.indices) {
            val input = set[i].input
            val target = set[i].output

            val update = (i + 1) % batchSize == 0 || (i + 1) == set.size

            val output = this.activate(input, true)
            this.propagate(rate, momentum, update, target)
            errorSum += cost.calc(target, output)
        }
        return errorSum / set.size
    }

    /**
     * Clear the context of the network
     */
    public fun clear(): Unit = this.nodes.forEach { it.clear() }

    /**
     * Backpropagation of the network
     */
    private fun propagate(rate: Double, momentum: Double, update: Boolean, target: DoubleArray) {
        if (target.size != this.output) {
            throw RuntimeException("Output target length should match network output length")
        }

        var targetIndex = target.size
        nodes.takeLast(this.output).reversed().forEach {
            it.propagate(rate, momentum, update, target[--targetIndex])
        }
        nodes.drop(this.input).dropLast(this.output).reversed().forEach {
            it.propagate(rate, momentum, update)
        }
    }


    /**
     * Activates the network
     * Nodes are activated chronologically
     *
     * @param input input array. array size must be equal to network input size
     * @param training default false
     *
     * @return result
     */
    @JsName("activate")
    public fun activate(input: DoubleArray, training: Boolean = false): DoubleArray {
        val output = mutableListOf<Double>()

        for ((i, node) in nodes.withIndex()) {
            when (node.type) {
                NodeTypeEnum.INPUT -> node.activate(input[i])
                NodeTypeEnum.OUTPUT -> output.add(node.activate())
                else -> {
                    if (training) node.mask = if (Random.nextDouble() < this.dropout) 0.0 else 1.0
                    node.activate()
                }
            }
        }
        return output.toDoubleArray()
    }

    /**
     * splits the network into two parts.
     * First will be of size [firstSize] and [firstOutputSize] output size
     * Second will be of remainder size and [output] output size
     */
    @JsName("disconnect")
    public fun split(firstSize: Int, firstOutputSize: Int): Pair<Network, Network> {
        val result1 = Network(input, firstOutputSize)
        val result2 = Network(firstOutputSize, output)
        val original = copy()
        result1.nodes.clear()
        result1.connections.clear()
        result2.nodes.clear()
        result2.connections.clear()

        result1.nodes.addAll(original.nodes.take(firstSize))
        result1.connections.addAll(original.connections.filter {
            result1.nodes.contains(it.from) && result1.nodes.contains(
                it.to
            )
        })
        result1.nodes.takeLast(firstOutputSize).forEach {
            it.type = NodeTypeEnum.OUTPUT
            it.connections.outputs.clear()
        }

        result2.nodes.addAll(original.nodes.takeLast(nodes.size - firstSize))
        result2.connections.addAll(original.connections.filter {
            result2.nodes.contains(it.from) && result2.nodes.contains(
                it.to
            )
        })
        result2.nodes.take(firstOutputSize).forEach {
            it.type = NodeTypeEnum.INPUT
            it.connections.inputs.clear()
        }
        return Pair(result1, result2)
    }

    public fun copy(): Network {
        val copyObject = serialize()

        return deserialize(copyObject)
    }

    public fun serialize(): CopyObject {
        val nodesCopy = mutableListOf<Node.NodeCopy>()
        val connectionsCopy = mutableListOf<ConnectionCopyObject>()

        nodes.indices.forEach { this.nodes[it].index = it }
        nodes.forEachIndexed { i, node ->
            val nodeCopy = node.copy()
            nodeCopy.index = i
            nodesCopy.add(nodeCopy)

            if (node.connections.self.weight != 0.0) {
                val connectionCopy =
                    ConnectionCopyObject(
                        from = i,
                        to = i,
                        gater = node.connections.self.gater?.index ?: -1,
                        weight = node.connections.self.weight
                    )
                connectionsCopy.add(connectionCopy)
            }
        }


        connectionsCopy.addAll(
            connections.map {
                ConnectionCopyObject(
                    weight = it.weight,
                    from = it.from.index,
                    to = it.to.index,
                    gater = it.gater?.index ?: -1
                )
            })


        return CopyObject(
            input = input,
            output = output,
            dropout = dropout,
            nodes = nodesCopy.toTypedArray(),
            connections = connectionsCopy.toTypedArray()
        )
    }

    @Serializable
    public data class CopyObject(
        val input: Int,
        val output: Int,
        val dropout: Double,
        val nodes: Array<Node.NodeCopy>,
        val connections: Array<ConnectionCopyObject>
    )

    @Serializable
    public data class ConnectionCopyObject(
        val from: Int,
        val to: Int,
        val gater: Int,
        val weight: Double
    )

    public companion object {
        @JsName("deserialize")
        public fun deserialize(copyObject: CopyObject): Network {
            val network = Network(
                copyObject.input,
                copyObject.output
            )
            network.dropout = copyObject.dropout
            network.nodes = mutableListOf()
            network.connections = mutableListOf()
            val nodes = copyObject.nodes
            val connections = copyObject.connections
            nodes.forEach {
                network.nodes.add(
                    Node(
                        bias = it.bias,
                        type = it.type,
                        squash = it.squash,
                        mask = it.mask
                    )
                )
            }

            connections.forEach {
                val connection = network.connect(network.nodes[it.from], network.nodes[it.to], it.weight)
                if (it.gater != -1) {
                    network.gate(network.nodes[it.gater], connection)
                }
            }
            return network
        }


        /**
         * Create an offspring from two parents
         */
        @JsName("crossover")
        public fun crossover(network1: Network, network2: Network, equal: Boolean): Network {
            if (network1.input != network2.input || network1.output != network2.output) {
                throw RuntimeException("Networks don't have the same input/output size!")
            }

            val offspring = Network(network1.input, network1.output)

            val score1 = if (network1.score.isNaN()) 0.0 else network1.score
            val score2 = if (network2.score.isNaN()) 0.0 else network2.score

            val size: Int = calcNewNetworkSize(equal, network1, network2)

            network1.nodes.forEachIndexed { index, node -> node.index = index }
            network2.nodes.forEachIndexed { index, node -> node.index = index }

            offspring.nodes = getOffspringNodes(size, network1, network2)

            val n1Conns = getConnectionsData(network1)
            val n2Conns = getConnectionsData(network2)

            val connections: MutableList<Conn> = arrayListOf()
            val keys1: List<Int> = n1Conns.keys.toList()
            val keys2: List<Int> = n2Conns.keys.toList()

            for (key in keys1.reversed()) {
                val n1con = n1Conns[key]!!
                val n2con = n2Conns[key]
                if (n2con != null) { // Common gene
                    val conn = if (Random.nextBoolean()) n1con else n2con
                    connections.add(conn)

                    n2Conns[key] = null
                } else if (score1 >= score2 || equal) {
                    connections.add(n1con)
                }
            }

            if (score2 >= score1 || equal) {
                connections.addAll(
                    keys2
                        .filter { n2Conns[it] != null }
                        .mapNotNull { n2Conns[it] }
                )
            }

            offspring.connections.clear()

            connections
                .filter { it.to < size && it.from < size }
                .forEach {
                    val from = offspring.nodes[it.from]
                    val to = offspring.nodes[it.to]
                    val connection = offspring.connect(from, to, it.weight)
                    if (it.gater != -1 && it.gater < size) {
                        offspring.gate(offspring.nodes[it.gater], connection)
                    }
                }

            return offspring
        }

        private fun getOffspringNodes(size: Int, network1: Network, network2: Network): ArrayList<Node> {
            val nodes = arrayListOf<Node>()
            for (i in 0 until size - network1.output) {
                val other: Node?
                var node: Node?
                if (Random.nextBoolean()) {
                    node = network1.nodes.getOrNull(i)
                    other = network2.nodes.getOrNull(i)
                } else {
                    node = network2.nodes.getOrNull(i)
                    other = network1.nodes.getOrNull(i)
                }

                if (node == null || node.type == NodeTypeEnum.OUTPUT) {
                    node = other
                    if (node == null) {
                        throw RuntimeException()
                    }
                }

                val newNode = Node(
                    bias = node.bias,
                    squash = node.squash,
                    type = node.type
                )
                nodes += newNode
            }
            for (i in size - network1.output until size) {
                val node = if (Random.nextBoolean())
                    network1.nodes[network1.nodes.size + i - size] else
                    network2.nodes[network2.nodes.size + i - size]
                val newNode = Node(
                    bias = node.bias,
                    squash = node.squash,
                    type = node.type
                )
                nodes += newNode
            }
            return nodes
        }

        private fun calcNewNetworkSize(equal: Boolean, network1: Network, network2: Network): Int {
            return if (equal || network1.score == network2.score) {
                val max = max(network1.nodes.size, network2.nodes.size)
                val min = min(network1.nodes.size, network2.nodes.size)
                floor(Random.nextDouble() * (max - min + 1) + min).toInt()
            } else if (network1.score > network2.score) {
                network1.nodes.size
            } else {
                network2.nodes.size
            }
        }

        private fun getConnectionsData(network: Network): MutableMap<Int, Conn?> {
            val conns: MutableMap<Int, Conn?> = mutableMapOf()
            for (connection in network.connections) {
                val data = Conn(
                    weight = connection.weight,
                    from = connection.from.index,
                    to = connection.to.index,
                    gater = connection.gater?.index ?: -1
                )
                conns[Connection.getInnovationID(connection.from.index, connection.to.index)] = data
            }
            for (connection in network.selfConns) {
                val data = Conn(
                    weight = connection.weight,
                    from = connection.from.index,
                    to = connection.to.index,
                    gater = connection.gater?.index ?: -1
                )
                conns[Connection.getInnovationID(connection.from.index, connection.to.index)] = data
            }
            return conns
        }

        /**
         * Merge two networks into one
         */
        @JsName("merge")
        public fun merge(first: Network, second: Network): Network {
            val network = Network(first.input, second.output)
            network.nodes.clear()
            network.connections.clear()
            network.gates.clear()
            network.selfConns.clear()

            val n1 = first.copy()
            val n2 = second.copy()
            val n1outputs = n1.nodes.filter { it.type == NodeTypeEnum.OUTPUT }
            val n2inputs = n2.nodes.filter { it.type == NodeTypeEnum.INPUT }
            network.nodes.addAll(n1.nodes)
            network.nodes.addAll(n2.nodes)
            network.connections.addAll(n1.connections)
            network.connections.addAll(n2.connections)
            network.gates.addAll(n1.gates)
            network.gates.addAll(n2.gates)
            network.selfConns.addAll(n1.selfConns)
            network.selfConns.addAll(n2.selfConns)
            n1outputs.forEachIndexed { index, node ->
                val to = n2inputs[index]
                to.squash = Activation.IDENTITY
                to.bias = 0.0
                to.connections.self.gain = 1.0
                to.connections.self.weight = 1.0
                network.connect(node, to, 1.0)
                node.type = NodeTypeEnum.HIDDEN
                to.type = NodeTypeEnum.CONSTANT
            }
            return network
        }
    }
}

private fun Network.sizePenalty(growth: Double): Double {
    return (nodes.size - input - output + connections.size + gates.size) * growth
}

package com.kalgecin.neataptic4k

import com.kalgecin.neataptic4k.layer.NodeGroup
import kotlinx.serialization.Serializable
import kotlin.math.floor
import kotlin.random.Random

public class Node(
        public var type: NodeTypeEnum = NodeTypeEnum.HIDDEN,
        public var index: Int = 0,
        public var squash: Activation = Activation.LOGISTIC,
        public var bias: Double = if (type == NodeTypeEnum.INPUT) 0.0 else Random.nextDouble(0.1, 0.2),
        public var mask: Double = 1.0
) {

    public val connections: ConnectionListNode = ConnectionListNode(self = Connection(this, this, 0.0))

    private var prevDeltaBias: Double = 0.0
    private var totalDeltaBias: Double = 0.0
    private var old: Double = 0.0
    private var state: Double = 0.0
    private var activation: Double = 0.0
    private var errorGated: Double = 0.0
    private var errorResponsibility: Double = 0.0
    private var errorProjected: Double = 0.0
    private var derivative: Double = 0.0

    public fun copy(): NodeCopy = NodeCopy(
        bias = bias,
        type = type,
        squash = squash,
        mask = mask
    )

    @Serializable
    public data class NodeCopy(
        val bias: Double,
        val type: NodeTypeEnum,
        val squash: Activation,
        val mask: Double,
        var index: Int = 0
    )

    public fun activate(): Double {
        old = state
        state = connections.self.gain * connections.self.weight * state + bias
        state += connections.inputs.sumByDouble { it.from.activation * it.weight * it.gain }

        activation = squash.calc(state) * mask
        derivative = squash.calc(state, true)

        val nodes: MutableList<Node> = arrayListOf()
        val influences: MutableList<Double> = connections.gated.indices.map { 0.0 }.toMutableList()

        for (connection in this.connections.gated) {
            val node: Node = connection.to

            val index = nodes.indexOf(node)
            if (index > -1) {
                influences[index] += connection.weight * connection.from.activation
            } else {
                nodes.add(node)
                val old = if (node.connections.self.gater == this) node.old else 0.0
                influences.add(connection.weight * connection.from.activation + old)
            }
            connection.gain = activation
        }

        for (connection in this.connections.inputs) {
            connection.eligibility =
                connections.self.gain * connections.self.weight * connection.eligibility + connection.from.activation * connection.gain

            for (j in nodes.indices) {
                val node: Node = nodes[j]
                val influence = influences[j]
                val index = connection.xTraceNodes.indexOf(node)
                if (index > -1) {
                    connection.xTraceValues[index] =
                        node.connections.self.gain * node.connections.self.weight * connection.xTraceValues[index] +
                                derivative * connection.eligibility * influence
                } else {
                    connection.xTraceNodes.add(node)
                    connection.xTraceValues.add(derivative * connection.eligibility * influence)
                }
            }
        }
        return this.activation
    }

    public fun activate(input: Double): Double {
        this.activation = input
        return this.activation
    }

    /**
     * Activates the node without calculating backpropagation
     */
    public fun noTraceActivation(): Double {
        state = connections.self.gain * connections.self.weight * state + bias
        state += connections.inputs.sumByDouble { it.from.activation * it.weight * it.gain }
        activation = squash.calc(state)
        connections.gated.forEach { it.gain = activation }
        return activation
    }

    public fun noTraceActivation(input: Double) {
        this.activation = input
    }

    /**
     * Back-propagate the error
     */
    public fun propagate(rate: Double = 0.3, momentum: Double = 0.0, update: Boolean, target: Double = 0.0) {

        if (type == NodeTypeEnum.OUTPUT) {
            errorResponsibility = target - activation
            errorProjected = target - activation
        } else {
            var error = connections.outputs.sumByDouble { it.to.errorResponsibility * it.weight * it.gain }
            errorProjected = derivative * error
            error = 0.0

            for (connection in this.connections.gated) {
                val node = connection.to
                var influence = if (node.connections.self.gater === this) node.old else 0.0
                influence += connection.weight * connection.from.activation
                error += node.errorResponsibility * influence
            }

            errorGated = derivative * error

            errorResponsibility = errorProjected * errorGated
        }

        if (type == NodeTypeEnum.CONSTANT) return

        connections.inputs.forEach { connection ->
            val gradient = errorProjected * connection.eligibility +
                    connection.xTraceNodes.indices
                        .sumByDouble { connection.xTraceNodes[it].errorResponsibility * connection.xTraceValues[it] }
            val deltaWeight = rate * gradient * mask
            connection.totalDeltaWeight += deltaWeight
            if (update) {
                connection.totalDeltaWeight += momentum * connection.prevDeltaWeight
                connection.weight += connection.totalDeltaWeight
                connection.prevDeltaWeight = connection.totalDeltaWeight
                connection.totalDeltaWeight = 0.0
            }
        }

        val deltaBias = rate * errorResponsibility
        totalDeltaBias += deltaBias
        if (update) {
            totalDeltaBias += momentum * prevDeltaBias
            bias += totalDeltaBias
            prevDeltaBias = totalDeltaBias
            totalDeltaBias = 0.0
        }
    }

    public fun connect(target: Node, weight: Double? = null): Connection {
        return when {
            target == this -> {
                if (connections.self.weight == 0.0) {
                    connections.self.weight = weight ?: 1.0
                }
                connections.self
            }
            this.isProjectingTo(target) -> throw RuntimeException("Already projecting a connection to this node!")
            else -> {
                val connection = Connection(this, target, weight ?: Random.nextDouble(0.1, 0.2))
                target.connections.inputs.add(connection)
                this.connections.outputs.add(connection)
                connection
            }
        }
    }

    public fun isProjectingTo(node: Node): Boolean {
        val any = connections.outputs.any { it.to == node }
        return this == node && this.connections.self.weight != 0.0 || any
    }

    private fun connect(target: NodeGroup, weight: Double): List<Connection> {
        val connections = ArrayList<Connection>()
        for (i in target.nodes.indices) {
            val connection = Connection(this, target.nodes[i], weight)
            target.nodes[i].connections.inputs.add(connection)
            this.connections.outputs.add(connection)
            target.connections.inputs.add(connection)

            connections.add(connection)
        }
        return connections
    }

    public fun disconnect(node: Node): Unit = this.disconnect(node, false)

    public fun clear() {
        for (connection in this.connections.inputs) {
            connection.eligibility = 0.0
            connection.xTraceNodes = arrayListOf()
            connection.xTraceValues = arrayListOf()
        }
        connections.gated.forEach { it.gain = 0.0 }
        this.errorResponsibility = 0.0
        this.errorProjected = 0.0
        this.errorGated = 0.0
        this.old = 0.0
        this.state = 0.0
        this.activation = 0.0
    }

    public fun disconnect(node: Node, twoSided: Boolean) {
        if (this == node) {
            connections.self.weight = 0.0
            return
        }
        for (connection in connections.outputs) {
            if (connection.to == node) {
                connections.outputs.remove(connection)
                connection.to.connections.inputs.remove(connection)
                connection.gater?.ungate(connection)
                break
            }
        }
        if (twoSided) {
            node.disconnect(this)
        }
    }

    public fun ungate(connection: Connection): Unit = this.ungate(arrayOf(connection))

    private fun ungate(connections: Array<Connection>) {
        for (connection in connections) {
            this.connections.gated.remove(connection)
            connection.gater = null
            connection.gain = 1.0
        }
    }

    public fun gate(connection: Connection): Unit = this.gate(arrayOf(connection))

    private fun gate(connections: Array<Connection>) {
        connections.forEach { connection ->
            this.connections.gated.add(connection)
            connection.gater = this
        }
    }

    public fun mutate(method: MutationType) {
        when (method) {
            MutationType.MOD_ACTIVATION -> {
                val allowed = method.allowed
                val index = allowed.indexOf(squash) + (floor(Random.nextDouble() * (allowed.size - 1)) + 1).toInt()
                this.squash = allowed[index % allowed.size]
            }
            MutationType.MOD_BIAS -> this.bias += Random.nextDouble(method.min, method.max)
            else -> {
            }
        }
    }

    public fun isProjectedBy(node: Node): Boolean {
        val any = this.connections.inputs.any { it.from == node }
        return node == this && this.connections.self.weight != 0.0 || any
    }

    override fun toString(): String {
        return "Node{type=$type, bias=$bias}"
    }

    override fun equals(other: Any?): Boolean = this === other

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + squash.hashCode()
        result = 31 * result + bias.hashCode()
        return result
    }
}

public data class NodeValues(
    val bias: Double? = null,
    val squash: Activation? = null,
    val type: NodeTypeEnum? = null
)

public data class ConnectionListNode(
    val inputs: MutableList<Connection> = mutableListOf(),
    val outputs: MutableList<Connection> = mutableListOf(),
    val gated: MutableList<Connection> = mutableListOf(),
    val self: Connection
)

public enum class NodeTypeEnum {
    HIDDEN, INPUT, OUTPUT, CONSTANT
}


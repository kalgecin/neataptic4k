package com.kalgecin.neataptic4k.layer

import com.kalgecin.neataptic4k.Connection
import com.kalgecin.neataptic4k.ConnectionType

internal class Dense(size: Int) : Layer() {
    private val block: NodeGroup =
        NodeGroup(size)

    override fun input(from: NodeGroup?, method: ConnectionType?, weight: Double): List<Connection> {
        return from?.connect(block, method ?: ConnectionType.ALL_TO_ALL, weight) ?: emptyList()
    }

    init {
        nodeGroups.add(block)
        output = block
    }
}

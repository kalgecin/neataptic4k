package com.kalgecin.neataptic4k.layer

import com.kalgecin.neataptic4k.*

internal class GRU(size: Int) : Layer() {
    private val resetGate: NodeGroup
    private val updateGate: NodeGroup = NodeGroup(size)
    private val memoryCell: NodeGroup

    override fun input(from: NodeGroup?, method: ConnectionType?, weight: Double): List<Connection> {
        val mmethod: ConnectionType? = method ?: ConnectionType.ALL_TO_ALL
        if (from == null) return emptyList()
        val connections: MutableList<Connection> = ArrayList(from.connect(updateGate, mmethod, weight))
        connections.addAll(from.connect(resetGate, mmethod, weight))
        connections.addAll(from.connect(memoryCell, mmethod, weight))

        return connections
    }

    init {
        val inverseUpdateGate = NodeGroup(size)
        resetGate = NodeGroup(size)
        memoryCell = NodeGroup(size)
        val output = NodeGroup(size)
        val previousOutput = NodeGroup(size)
        previousOutput.set(
            NodeValues(
                bias = 0.0,
                squash = Activation.IDENTITY,
                type = NodeTypeEnum.CONSTANT
            )
        )
        memoryCell.set(NodeValues(squash = Activation.TANH))
        inverseUpdateGate.set(
            NodeValues(
                bias = 0.0,
                squash = Activation.INVERSE,
                type = NodeTypeEnum.CONSTANT
            )
        )
        updateGate.set(NodeValues(bias = 1.0))
        resetGate.set(NodeValues(bias = 0.0))
        previousOutput.connect(updateGate, ConnectionType.ALL_TO_ALL)
        updateGate.connect(inverseUpdateGate, ConnectionType.ONE_TO_ONE, 1.0)
        previousOutput.connect(resetGate, ConnectionType.ALL_TO_ALL)
        val reset = previousOutput.connect(
            memoryCell,
            ConnectionType.ALL_TO_ALL
        )
        resetGate.gate(reset, GatingType.OUTPUT)
        val update1 = previousOutput.connect(
            output,
            ConnectionType.ALL_TO_ALL
        )
        val update2 = memoryCell.connect(output, ConnectionType.ALL_TO_ALL)
        updateGate.gate(update1, GatingType.OUTPUT)
        inverseUpdateGate.gate(update2, GatingType.OUTPUT)
        output.connect(previousOutput, ConnectionType.ONE_TO_ONE, 1.0)
        nodeGroups = mutableListOf(updateGate, inverseUpdateGate, resetGate, memoryCell, output, previousOutput)
        this.output = output
    }
}

package com.kalgecin.neataptic4k.layer

import com.kalgecin.neataptic4k.*

internal abstract class Layer : NodeGroup(0) {
    var output: NodeGroup? = null
    var nodeGroups: MutableList<NodeGroup> = arrayListOf()

    override fun gate(connections: List<Connection>, method: GatingType?) {
        this.output?.gate(connections, method)
    }

    override fun connect(target: NodeGroup, method: ConnectionType?, weight: Double?): List<Connection> {
        return this.output?.connect(target, method, weight) ?: arrayListOf()
    }

    private fun disconnect(target: NodeGroup, twoSided: Boolean) {
        for (nodeGroup in this.nodeGroups) {
            for (j in target.nodes.indices) {
                this.nodeGroups[j].disconnect(target.nodes[j], twoSided)

                for (k in this.connections.out.indices.reversed()) {
                    val connection = this.connections.out[k]

                    if (connection.from == nodeGroup.nodes[0] && connection.to == target.nodes[j]) {
                        this.connections.inputs.removeAt(k)
                    }
                }

                if (twoSided) {
                    for (k in (this.connections.inputs.size - 1) until 0) {
                        val connection = this.connections.inputs[k]
                        if (connection.from == target.nodes[j] && connection.to == nodeGroup.nodes[0]) {
                            this.connections.inputs.removeAt(k)
                        }
                    }
                }
            }
        }
    }

    override fun disconnect(target: Node, twoSided: Boolean) {
        for (nodeGroup in this.nodeGroups) {
            nodeGroup.disconnect(target, twoSided)
            for (j in this.connections.out.indices.reversed()) {
                val connection = this.connections.out[j]

                if (connection.from == nodeGroup.nodes[0] && connection.to == target) {
                    this.connections.out.removeAt(j)
                    break
                }
            }
            if (twoSided) {
                for (k in this.connections.inputs.indices.reversed()) {
                    val connection = this.connections.inputs[k]
                    if (connection.from == target && connection.to == nodeGroup.nodes[0]) {
                        this.connections.inputs.removeAt(k)
                        break
                    }
                }
            }
        }
    }

    override fun activate(value: DoubleArray?): DoubleArray {
        if (value != null && value.size != this.nodeGroups.size) {
            throw RuntimeException("Array with values should be same as the amount of nodes!")
        }
        return this.nodeGroups.indices
            .map { i ->
                if (value == null) this.nodeGroups[i].nodes[0].activate() else this.nodeGroups[i].nodes[0].activate(value[i])
            }
            .toDoubleArray()
    }

    override fun propagate(rate: Double, momentum: Double, target: DoubleArray?) {
        if (target != null && target.size != this.nodeGroups.size) {
            throw RuntimeException("Array with values should be same as the amount of nodes!")
        }

        for (i in this.nodeGroups.indices.reversed()) {
            if (target == null) {
                this.nodeGroups[i].nodes[0].propagate(rate, momentum, true)
            } else {
                this.nodeGroups[i].nodes[0].propagate(rate, momentum, true, target[i])
            }
        }
    }

    override fun disconnect(target: Node) = this.disconnect(target, false)

    override fun connect(target: Layer, method: ConnectionType?, weight: Double) {
        target.input(this, method, weight)
    }

    override fun connect(target: Node, method: ConnectionType, weight: Double): List<Connection> {
        return this.output?.connect(target, method, weight) ?: arrayListOf()
    }

    override fun set(values: NodeValues) = this.nodeGroups.forEach { node -> node.set(values) }

    override fun clear() = this.nodeGroups.forEach { it.clear() }

    fun input(layer: Layer, method: ConnectionType, weight: Double) = this.input(layer.output, method, weight)


    abstract fun input(from: NodeGroup?, method: ConnectionType?, weight: Double): List<Connection>
}

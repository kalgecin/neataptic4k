package com.kalgecin.neataptic4k.layer

import com.kalgecin.neataptic4k.*

internal class Memory(size: Int, memory: Int) : Layer() {
    override fun input(from: NodeGroup?, method: ConnectionType?, weight: Double): List<Connection> {
        if(from == null ) return emptyList()
        if (from.nodes.size != nodeGroups[nodes.size - 1].nodes.size) {
            throw RuntimeException("Previous layer size must be same as memory size")
        }
        return from.connect(nodes[nodes.size - 1],
            ConnectionType.ONE_TO_ONE, 1.0)
    }

    init {
        var previous: NodeGroup? = null
        for (i in 0 until memory) {
            val block = NodeGroup(size)
            block.set(
                NodeValues(
                    squash = Activation.IDENTITY,
                    bias = 0.0,
                    type = NodeTypeEnum.CONSTANT
                )
            )
            previous?.connect(block, ConnectionType.ONE_TO_ONE, 1.0)
            nodeGroups.add(block)
            previous = block
        }
        nodeGroups.reverse()
        nodeGroups.map { node: NodeGroup -> node.nodes }.forEach { it.reverse() }
        val outputGroup = NodeGroup(0)
        for (group in nodeGroups) outputGroup.nodes.addAll(group.nodes)
        output = outputGroup
    }
}

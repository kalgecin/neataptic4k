package com.kalgecin.neataptic4k.layer

import com.kalgecin.neataptic4k.*
import kotlin.random.Random

internal open class NodeGroup(size: Int, activationFunction: Activation = Activation.LOGISTIC) {
    val connections: ConnectionListGroup = ConnectionListGroup()
    val nodes: MutableList<Node> = (0 until  size).map { Node(squash = activationFunction) }.toMutableList()

    open fun gate(connections: List<Connection>, method: GatingType?) {
        if (method == null) {
            throw RuntimeException("Please specify Gating.INPUT, Gating.OUTPUT")
        }
        val nodes1 = arrayListOf<Node>()
        val nodes2 = arrayListOf<Node>()
        for (connection in connections) {
            if (!nodes1.contains(connection.from)) {
                nodes1.add(connection.from)
            }
            if (!nodes2.contains(connection.to)) {
                nodes2.add(connection.to)
            }
        }

        when (method) {
            GatingType.INPUT ->
                for (i in nodes2.indices) {
                    val gater = this.nodes[i % this.nodes.size]
                    nodes2[i].connections.inputs.filter { connections.contains(it) }.forEach { gater.gate(it) }
                }
            GatingType.OUTPUT ->
                for (i in nodes1.indices) {
                    val gater = this.nodes[i % this.nodes.size]
                    nodes1[i].connections.outputs.filter { connections.contains(it) }.forEach { gater.gate(it) }
                }
            GatingType.SELF ->
                for (i in nodes1.indices) {
                    val node = nodes1[i]
                    if (connections.contains(node.connections.self)) {
                        this.nodes[i % this.nodes.size].gate(node.connections.self)
                    }
                }
        }
    }

    open fun connect(target: NodeGroup, method: ConnectionType?, weight: Double? = null): List<Connection> {
        val connectionType = method ?: if (this == target) ConnectionType.ONE_TO_ONE else ConnectionType.ALL_TO_ALL
        val connections: MutableList<Connection> = mutableListOf()
        if (connectionType == ConnectionType.ALL_TO_ALL || connectionType == ConnectionType.ALL_TO_ELSE) {
            for (node in nodes) {
                for (targetNode in target.nodes) {
                    if (connectionType == ConnectionType.ALL_TO_ELSE && node == targetNode) {
                        continue
                    }
                    val connection = node.connect(targetNode, weight)
                    this.connections.out.add(connection)
                    target.connections.inputs.add(connection)
                    connections.add(connection)
                }
            }
        } else if (connectionType == ConnectionType.ONE_TO_ONE) {
            if (nodes.size != target.nodes.size) {
                throw RuntimeException("From and To group must be the same size!")
            }
            for (i in nodes.indices) {
                val connection = nodes[i].connect(target.nodes[i], weight)
                this.connections.self.add(connection)
                connections.add(connection)
            }
        }
        return connections
    }

    open fun disconnect(target: Node) = this.disconnect(target, false)

    open fun disconnect(target: Node, twoSided: Boolean) {
        for (node in this.nodes) {
            node.disconnect(target, twoSided)

            for (j in this.connections.out.indices.reversed()) {
                val connection = this.connections.out[j]

                if (connection.from == node && connection.to == target) {
                    this.connections.out.removeAt(j)
                    break
                }
            }
            if (twoSided) {
                for (j in this.connections.inputs.indices.reversed()) {
                    val connection = this.connections.inputs[j]

                    if (connection.from == target && connection.to == node) {
                        this.connections.inputs.removeAt(j)
                        break
                    }
                }
            }
        }
    }

    open fun activate(value: DoubleArray?): DoubleArray {
        if (value != null && value.size != this.nodes.size) {
            throw RuntimeException("Array with values should be same as the amount of nodes!")
        }
        return (0 until  this.nodes.size)
            .map { i -> if (value == null) this.nodes[i].activate() else this.nodes[i].activate(value[i]) }
            .toDoubleArray()
    }

    open fun propagate(rate: Double, momentum: Double, target: DoubleArray?) {
        if (target != null && target.size != this.nodes.size) {
            throw RuntimeException("Array with values should be same as the amount of nodes!")
        }

        for (i in this.nodes.indices.reversed()) {
            if (target == null) {
                this.nodes[i].propagate(rate, momentum, true)
            } else {
                this.nodes[i].propagate(rate, momentum, true, target[i])
            }
        }
    }

    open fun connect(target: Layer, method: ConnectionType?, weight: Double) {
        target.input(this, method, weight)
    }

    open fun connect(target: Node, method: ConnectionType, weight: Double): List<Connection> {
        val connections = arrayListOf<Connection>()
        this.nodes.map { node -> node.connect(target, weight) }
            .forEach { connection ->
                this.connections.out.add(connection)
                connections.add(connection)
            }
        return connections
    }

    open fun set(values: NodeValues) {
        this.nodes.forEach { node ->
            if (values.bias != null) {
                node.bias = values.bias
            }
            node.squash = values.squash ?: node.squash
            node.type = values.type ?: node.type
        }
    }

    open fun clear() = this.nodes.forEach { it.clear() }
}

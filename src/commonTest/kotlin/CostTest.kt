package com.kalgecin.neataptic4k

import kotlin.test.Test
import kotlin.test.assertEquals

class CostTest {
    @Test
    fun testSMCEL() {
        val a = doubleArrayOf(1.0,0.0,0.0)
        val b = doubleArrayOf(0.5,0.4,0.1)
        val result = Cost.SMCEL.calc(a, b)
        assertEquals(0.9459106833418139, result)
    }
}
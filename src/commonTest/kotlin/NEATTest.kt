package com.kalgecin.neataptic4k

import com.kalgecin.neataptic4k.MutationType.Companion.FFW
import kotlin.test.Test
import kotlin.test.assertTrue

class NEATTest {
    @Test
    fun testAND() {
        val trainingSet = arrayOf(
            DataEntry(
                doubleArrayOf(0.0, 0.0),
                doubleArrayOf(0.0)
            ),
            DataEntry(
                doubleArrayOf(0.0, 1.0),
                doubleArrayOf(0.0)
            ),
            DataEntry(
                doubleArrayOf(1.0, 0.0),
                doubleArrayOf(0.0)
            ),
            DataEntry(
                doubleArrayOf(1.0, 1.0),
                doubleArrayOf(1.0)
            )
        )
        val network = Network(2, 1)
        val options = EvolveOptions(
            mutationTypes = FFW,
            isEqual = true,
            elitism = 10,
            mutationRate = 0.5,
            error = 0.03
        )
        assertTrue(network.evolve(trainingSet, options) <= 0.03)
    }

    @Test
    fun testXOR() {
        val trainingSet = arrayOf(
            DataEntry(
                doubleArrayOf(0.0, 0.0),
                doubleArrayOf(0.0)
            ),
            DataEntry(
                doubleArrayOf(0.0, 1.0),
                doubleArrayOf(1.0)
            ),
            DataEntry(
                doubleArrayOf(1.0, 0.0),
                doubleArrayOf(1.0)
            ),
            DataEntry(
                doubleArrayOf(1.0, 1.0),
                doubleArrayOf(0.0)
            )
        )
        val network = Network(2, 1)
        val options = EvolveOptions(
            mutationTypes = FFW,
            isEqual = true,
            elitism = 10,
            mutationRate = 0.5,
            error = 0.03
        )
        assertTrue(network.evolve(trainingSet, options) <= 0.03)
    }

    @Test
    fun testXNOR() {
        val trainingSet = arrayOf(
            DataEntry(
                doubleArrayOf(0.0, 0.0),
                doubleArrayOf(1.0)
            ),
            DataEntry(
                doubleArrayOf(0.0, 1.0),
                doubleArrayOf(0.0)
            ),
            DataEntry(
                doubleArrayOf(1.0, 0.0),
                doubleArrayOf(0.0)
            ),
            DataEntry(
                doubleArrayOf(1.0, 1.0),
                doubleArrayOf(1.0)
            )
        )
        val network = Network(2, 1)
        val options = EvolveOptions(
            mutationTypes = FFW,
            isEqual = true,
            elitism = 10,
            mutationRate = 0.5,
            error = 0.03
        )
        assertTrue(network.evolve(trainingSet, options) <= 0.03)
    }
}

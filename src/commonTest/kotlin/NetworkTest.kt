package com.kalgecin.neataptic4k

import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random
import kotlin.test.*

class NetworkTest {
    @Test
    fun testAndGate() {
        val set = arrayOf(
            DataEntry(
                input = doubleArrayOf(
                    0.0,
                    0.0
                ), output = doubleArrayOf(0.0)
            ),
            DataEntry(
                input = doubleArrayOf(
                    0.0,
                    1.0
                ), output = doubleArrayOf(0.0)
            ),
            DataEntry(
                input = doubleArrayOf(
                    1.0,
                    0.0
                ), output = doubleArrayOf(0.0)
            ),
            DataEntry(
                input = doubleArrayOf(
                    1.0,
                    1.0
                ), output = doubleArrayOf(1.0)
            )
        )
        learnSet(set, 1000, 0.002)
    }

    @Test
    fun testXORGate() {
        learnSet(
            arrayOf(
                DataEntry(
                    doubleArrayOf(0.0, 0.0),
                    doubleArrayOf(0.0)
                ),
                DataEntry(
                    doubleArrayOf(0.0, 1.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0, 0.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0, 1.0),
                    doubleArrayOf(0.0)
                )
            ), 10000, 0.002
        )
    }

    @Test
    fun testORGate() {
        learnSet(
            arrayOf(
                DataEntry(
                    doubleArrayOf(0.0, 0.0),
                    doubleArrayOf(0.0)
                ),
                DataEntry(
                    doubleArrayOf(0.0, 1.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0, 0.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0, 1.0),
                    doubleArrayOf(1.0)
                )
            ), 1000, 0.002
        )
    }

    @Test
    fun testSIN() {
        val set = arrayOfNulls<DataEntry>(100)
        for (i in set.indices) {
            val inputValue: Double = Random.nextDouble() * PI * 2
            set[i] = DataEntry(
                doubleArrayOf(inputValue / (PI * 2)),
                doubleArrayOf((sin(inputValue) + 1) / 2)
            )
        }
        learnSet(set.filterNotNull().toTypedArray(), 1000, 0.05)
    }

    @Test
    fun testBiggerThan() {
        val set = arrayOfNulls<DataEntry>(100)
        for (i in set.indices) {
            val x: Double = Random.nextDouble()
            val y: Double = Random.nextDouble()
            val z: Double = if (x > y) 1.0 else 0.0
            set[i] = DataEntry(
                doubleArrayOf(x, y),
                doubleArrayOf(z)
            )
        }
        learnSet(set.filterNotNull().toTypedArray(), 500, 0.05)
    }

    private fun learnSet(set: Array<DataEntry>, iterations: Int, error: Double) {
        val network = Architect.createPerceptron(set[0].input.size, 5, set[0].output.size)
        val options = TrainOptions(
            iterations = iterations,
            error = error,
            shuffle = true,
            rate = 0.05,
            momentum = 0.9
        )
        assertTrue(network.train(set, options) <= error)
    }

    @Test
    fun testGRU_XOR() {
        val gru: Network = Architect.createGRU(1, 2, 1)
        val options = TrainOptions(
            error = 0.001,
            iterations = 5000,
            rate = 0.1,
            clear = true
        )
        gru.nodes.forEach {
            it.connections.self.weight = 0.0
            it.connections.gated.forEach { c -> c.weight = 0.0 }
        }
        gru.train(
            arrayOf(
                DataEntry(
                    doubleArrayOf(0.0),
                    doubleArrayOf(0.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0),
                    doubleArrayOf(0.0)
                ),
                DataEntry(
                    doubleArrayOf(0.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(0.0),
                    doubleArrayOf(0.0)
                )
            ), options
        )
        gru.activate(doubleArrayOf(0.0))
        assertTrue(0.9 < gru.activate(doubleArrayOf(1.0))[0], "GRU Error")
        assertTrue(gru.activate(doubleArrayOf(1.0))[0] < 0.1, "GRU Error")
        assertTrue(0.9 < gru.activate(doubleArrayOf(0.0))[0], "GRU Error")
        assertTrue(gru.activate(doubleArrayOf(0.0))[0] < 0.1, "GRU Error")
    }

    @Test
    fun testSIN_COS() {
        val set = arrayOfNulls<DataEntry>(100)
        for (i in set.indices) {
            val inputValue: Double = Random.nextDouble() * PI * 2
            set[i] = DataEntry(
                doubleArrayOf(inputValue / (PI * 2)),
                doubleArrayOf((sin(inputValue) + 1) / 2, (cos(inputValue) + 1) / 2)
            )
        }
        learnSet(set.filterNotNull().toTypedArray(), 1000, 0.05)
    }

    @Test
    fun testSHIFT() {
        val set = arrayOfNulls<DataEntry>(1000)
        for (i in set.indices) {
            val x: Double = Random.nextDouble()
            val y: Double = Random.nextDouble()
            val z: Double = Random.nextDouble()
            set[i] = DataEntry(
                doubleArrayOf(x, y, z),
                doubleArrayOf(z, x, y)
            )
        }
        learnSet(set.filterNotNull().toTypedArray(), 500, 0.03)
    }

    @Test
    fun testNOTGate() {
        learnSet(
            arrayOf(
                DataEntry(
                    doubleArrayOf(0.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0),
                    doubleArrayOf(0.0)
                )
            ), 1000, 0.002
        )
    }

    @Test
    fun testXNORGate() {
        learnSet(
            arrayOf(
                DataEntry(
                    doubleArrayOf(0.0, 0.0),
                    doubleArrayOf(1.0)
                ),
                DataEntry(
                    doubleArrayOf(0.0, 1.0),
                    doubleArrayOf(0.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0, 0.0),
                    doubleArrayOf(0.0)
                ),
                DataEntry(
                    doubleArrayOf(1.0, 1.0),
                    doubleArrayOf(1.0)
                )
            ), 10000, 0.002
        )
    }

    @Test
    fun feedForward() {
        val network1 = Network(2, 2)
        val network2 = Network(2, 2)
        for (i in 0..99) {
            network1.mutate(MutationType.ADD_NODE)
            network2.mutate(MutationType.ADD_NODE)
        }
        for (i in 0..399) {
            network1.mutate(MutationType.ADD_CONN)
            network2.mutate(MutationType.ADD_CONN)
        }
        val network = Network.crossover(network1, network2, false)
        for (i in network.connections.indices) {
            val from = network.nodes.indexOf(network.connections[i].from)
            val to = network.nodes.indexOf(network.connections[i].to)
            assertTrue(from < to, "Network is not feeding forward correctly")
        }
    }

    @Test
    fun networkTest() {
        val network = Architect.createGRU(2, 4, 4, 4)
        assertNotNull(network)
    }

    @Test
    fun mutate() {
        for (mutationType in MutationType.ALL) {
            checkMutation(mutationType)
        }
    }

    private fun checkMutation(method: MutationType) {
        val network = Architect.createPerceptron(2, 4, 4, 4, 2)
        network.mutate(MutationType.ADD_GATE)
        network.mutate(MutationType.ADD_BACK_CONN)
        network.mutate(MutationType.ADD_SELF_CONN)
        val originalOutput: MutableList<Array<Double>> = mutableListOf()
        for (i in 0..10) {
            for (j in 0..10) {
                originalOutput.add(
                    network.activate(
                        doubleArrayOf(i.toDouble() / 10, j.toDouble() / 10),
                        false
                    ).toTypedArray()
                )
            }
        }
        network.mutate(method)
        val mutatedOutput: MutableList<Array<Double>> = mutableListOf()
        for (i in 0..100) {
            for (j in 0..100) {
                mutatedOutput.add(
                    network.activate(
                        doubleArrayOf(i.toDouble() / 50, j.toDouble() / 50),
                        false
                    ).toTypedArray()
                )
            }
        }
        val isEqual: Boolean = originalOutput.toTypedArray().contentDeepEquals(mutatedOutput.toTypedArray())
        assertFalse(isEqual)
    }

    @Test
    fun testCopy() {
        val network = Architect.createPerceptron(2, 4, 4, 4, 2)
        val copy = network.copy()
        assertTrue(network !== copy)
        assertEquals(copy.connections.size, network.connections.size)
        assertEquals(copy.gates.size, network.gates.size)
        assertEquals(copy.input, network.input)
        assertEquals(copy.output, network.output)
        assertEquals(copy.nodes.size, network.nodes.size)
        assertEquals(copy.selfConns.size, network.selfConns.size)
    }

    @Test
    fun testFahToKelvin() {
        val network = Network(1, 1)
        val inputs = doubleArrayOf(-200.0, -90.0, -40.0, 14.0, 32.0, 46.0, 59.0, 72.0, 100.0)
        val outputs = doubleArrayOf(144.261, 205.372, 233.15, 263.15, 273.15, 280.928, 288.15, 295.372, 310.928)
        val dataSet = inputs.indices.map { i -> DataEntry(doubleArrayOf(inputs[i]), doubleArrayOf(outputs[i])) }.toTypedArray()
        val error = 0.01
        network.nodes.takeLast(network.output).forEach { it.squash = Activation.BENT_IDENTITY }
        network.train(
            dataSet,
            TrainOptions(iterations = 9000, log = 1000, error = 0.0, rate = 0.000001, momentum = 0.99)
        )
        val one = network.activate(doubleArrayOf(-50.0))[0]
        val two = network.activate(doubleArrayOf(0.0))[0]
        val three = network.activate(doubleArrayOf(50.0))[0]

        println("-50: $one -> (${(one - 227.594)})")
        println("0: $two -> (${(two - 255.372)})")
        println("50: $three -> (${(three - 283.15)})")

        assertTrue(abs(one - 227.594) <= error)
        assertTrue(abs(two - 255.372) <= error)
        assertTrue(abs(three - 283.15) <= error)

        println(network.serialize())
    }

    @Test
    fun testLSTM() {
        val text = "Am I conscious? Or am I not?".toLowerCase()
        val characters = text.toCharArray().distinct()
        val onehot: MutableMap<Char, DoubleArray> = mutableMapOf()
        characters.forEachIndexed { index, s ->
            val zeros = characters.indices.map { 0.0 }.toDoubleArray()
            zeros[index] = 1.0
            onehot[s] = zeros
        }
        val dataset = mutableListOf<DataEntry>()
        var prev = text[0]
        text.drop(1).forEach {
            dataset.add(
                DataEntry(
                    input = onehot[prev]!!, output = onehot[it]!!
                ))
            prev = it
        }
        val network = Architect.createLSTM(emptyMap(), characters.size, 10, characters.size)
        println("Network conns: ${network.connections.size}, nodes: ${network.nodes.size}")
        println("Dataset size: ${dataset.size}")
        println("Characters: ${onehot.keys.size}")

        network.train(dataset.toTypedArray(), TrainOptions(
            log = 1,
            rate = 0.1,
            cost = Cost.MSE,
            error = 0.004,
            clear = true
        ))

        var outputText = ""
        network.clear()
        var output = network.activate(dataset[0].input)
        outputText += text[0]
        repeat(text.length - 1) {
            val index = output.withIndex().maxByOrNull { (_, v) -> v }?.index!!
            val zeros = characters.indices.map { 0.0 }.toDoubleArray()
            zeros[index] = 1.0
            val char = onehot.keys.first { entry -> onehot[entry].contentEquals(zeros) }
            outputText += char
            output = network.activate(zeros)
        }
        println("'$outputText'")
        println("'$text'")
        assertEquals(text, outputText)
    }

    @Test
    fun testMergeAndDisconnect() {
        val network1 = Architect.createPerceptron(5, 3, 1)
        val network2 = Architect.createPerceptron(1, 3, 5)
        val merged = Network.merge(network1, network2)
        assertEquals(network1.input, merged.input)
        assertEquals(network2.output, merged.output)
        assertEquals(network1.nodes.size + network2.nodes.size, merged.nodes.size)
        assertEquals(network1.connections.size + network2.connections.size + network2.input, merged.connections.size)
        val in1 = (0 until 5).map { Random.nextDouble() }.toDoubleArray()
        val in2 = network1.noTraceActivate(in1)
        val out2 = network2.activate(in2)
        val outMerged = merged.noTraceActivate(in1)
        assertTrue(out2.contentEquals(outMerged))

        val disconnected = merged.split(network1.nodes.size, network1.output)
        val outD1 = disconnected.first.noTraceActivate(in1)
        assertTrue(in2.contentEquals(outD1))
        val outD2 = disconnected.second.noTraceActivate(in2)
        assertTrue(out2.contentEquals(outD2))

    }

}

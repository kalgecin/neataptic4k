package com.kalgecin.neataptic4k

internal actual fun NEAT.evaluateNeat(population: Array<Network>, fitnessFunction: ((Network) -> Double)?, options: EvolveOptions) {
    population.map { genome ->
        if (options.isClear) genome.clear()
        genome.score = fitnessFunction?.let { it(genome) } ?: 0.0
    }
}
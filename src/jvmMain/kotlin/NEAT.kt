package com.kalgecin.neataptic4k

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

internal actual fun NEAT.evaluateNeat(population: Array<Network>, fitnessFunction: ((Network) -> Double)?, options: EvolveOptions) {
    runBlocking {
        withContext(Dispatchers.Default) {
            population.map { genome ->
                async {
                    genome.clear()
                    genome.score = fitnessFunction?.invoke(genome) ?: 0.0
                }
            }.forEach { it.await() }
        }
    }
}